from pprint import pprint
import sympy as sp
import asirt

l_1, l_2, l_3 = sp.symbols('l_1, l_2, l_3')
substitutions = {l_1: 3, l_2: 2, l_3: 2}  # for robot.plot()
robot = asirt.SerialLink(subs=substitutions)
robot.add_revolute_joint(0, 0, l_1, 0)
robot.add_revolute_joint(0, 0, l_2, 0)
# robot.plot()
kinetic_energy = robot.kinetic_energy()
pprint(kinetic_energy)

r1 = sp.Matrix([[-sp.symbols('l_1') / 2], [0], [0], [1]])
r2 = sp.Matrix([[-sp.symbols('l_2') / 2], [0], [0], [1]])
r = [r1, r2]
gravity_vector = sp.Matrix([[0, -sp.symbols('g'), 0, 0]])
potential_energy = robot.potential_energy(gravity_vector, r)
pprint(potential_energy)
