from pprint import pprint
import numpy as np
import sympy as sp
from scipy import linalg
from matplotlib import pyplot as plt
from mpl_toolkits import mplot3d


def simple_float(symbol, subs):
    return float(symbol.evalf(subs=subs)) if isinstance(symbol, sp.Symbol) else symbol


def rotx(angle):
    c = float(sp.cos(angle).evalf())
    s = float(sp.sin(angle).evalf())
    return np.array([[1, 0, 0],
                     [0, c, -s],
                     [0, s, c]])


def trotx(angle):
    c = float(sp.cos(angle).evalf())
    s = float(sp.sin(angle).evalf())
    return np.array([[1, 0, 0, 0],
                     [0, c, -s, 0],
                     [0, s, c, 0],
                     [0, 0, 0, 1]])


def sym_rotx(angle):
    return sp.Matrix([[1, 0, 0],
                      [0, sp.cos(angle), -sp.sin(angle)],
                      [0, sp.sin(angle), sp.cos(angle)]])


def sym_trotx(angle):
    return sp.Matrix([[1, 0, 0, 0],
                      [0, sp.cos(angle), -sp.sin(angle), 0],
                      [0, sp.sin(angle), sp.cos(angle), 0],
                      [0, 0, 0, 1]])


def roty(angle):
    c = float(sp.cos(angle).evalf())
    s = float(sp.sin(angle).evalf())
    return np.array([[c, 0, s],
                     [0, 1, 0],
                     [-s, 0, c]])


def troty(angle):
    c = float(sp.cos(angle).evalf())
    s = float(sp.sin(angle).evalf())
    return np.array([[c, 0, s, 0],
                     [0, 1, 0, 0],
                     [-s, 0, c, 0],
                     [0, 0, 0, 1]])


def sym_roty(angle):
    return sp.Matrix([[sp.cos(angle), 0, sp.sin(angle)],
                      [0, 1, 0],
                      [-sp.sin(angle), 0, sp.cos(angle)]])


def sym_troty(angle):
    return sp.Matrix([[sp.cos(angle), 0, sp.sin(angle), 0],
                      [0, 1, 0, 0],
                      [-sp.sin(angle), 0, sp.cos(angle), 0],
                      [0, 0, 0, 1]])


def rotz(angle):
    c = float(sp.cos(angle).evalf())
    s = float(sp.sin(angle).evalf())
    return np.array([[c, -s, 0],
                     [s, c, 0],
                     [0, 0, 1]])


def trotz(angle):
    c = float(sp.cos(angle).evalf())
    s = float(sp.sin(angle).evalf())
    return np.array([[c, -s, 0, 0],
                     [s, c, 0, 0],
                     [0, 0, 1, 0],
                     [0, 0, 0, 1]])


def sym_rotz(angle):
    return sp.Matrix([[sp.cos(angle), -sp.sin(angle), 0],
                      [sp.sin(angle), sp.cos(angle), 0],
                      [0, 0, 1]])


def sym_trotz(angle):
    return sp.Matrix([[sp.cos(angle), -sp.sin(angle), 0, 0],
                      [sp.sin(angle), sp.cos(angle), 0, 0],
                      [0, 0, 1, 0],
                      [0, 0, 0, 1]])


def translation(translation_vector):
    return np.array([[1, 0, 0, translation_vector[0]],
                     [0, 1, 0, translation_vector[1]],
                     [0, 0, 1, translation_vector[2]],
                     [0, 0, 0, 1]])


def sym_translation(translation_vector):
    return sp.Matrix([[1, 0, 0, translation_vector[0]],
                      [0, 1, 0, translation_vector[1]],
                      [0, 0, 1, translation_vector[2]],
                      [0, 0, 0, 1]])


def jacobian(outputs, inputs):
    j = sp.Matrix([[sp.diff(output, actuator) for actuator in inputs] for output in outputs])
    return sp.simplify(j)


def delta_collapse(transformation):
    assert transformation[0, 1] == -transformation[1, 0]
    assert transformation[0, 2] == -transformation[2, 0]
    assert transformation[1, 2] == -transformation[2, 1]
    assert transformation[0, 0] == 0
    assert transformation[1, 1] == 0
    assert transformation[2, 2] == 0
    assert transformation[3, 3] == 0
    assert transformation[3, 2] == 0
    assert transformation[3, 1] == 0
    assert transformation[3, 0] == 0
    return [transformation[2, 1], transformation[0, 2], transformation[1, 0],
            transformation[0, 3], transformation[1, 3], transformation[2, 3]]


def delta_expand(drot_x, drot_y, drot_z, dx, dy, dz):
    return np.array([[0, -drot_z, drot_y, dx],
                     [drot_z, 0, -drot_x, dy],
                     [-drot_y, drot_x, 0, dz],
                     [0, 0, 0, 0]])


def sym_delta_expand(drot_x, drot_y, drot_z, dx, dy, dz):
    return sp.Matrix([[0, -drot_z, drot_y, dx],
                      [drot_z, 0, -drot_x, dy],
                      [-drot_y, drot_x, 0, dz],
                      [0, 0, 0, 0]])


class SerialLink:
    def __init__(self, subs=None):
        self.links = []
        self.actuators = []
        self.revolute_joint_color = 'xkcd:sunflower yellow'
        self.prismatic_joint_color = 'xkcd:light light blue'
        self.subs = {} if subs is None else subs

    def add_revolute_joint(self, theta, d, a, alpha):
        self.links.append(('revolute', theta, d, a, alpha))
        self.actuators.append(sp.symbols('\u03B8_{}'.format(len(self.links))))

    def add_prismatic_joint(self, theta, d, a, alpha):
        self.links.append(('prismatic', theta, d, a, alpha))
        self.actuators.append(sp.symbols('d_{}'.format(len(self.links))))

    def reset(self):
        self.links = []
        self.actuators = []
        self.subs = {}

    @staticmethod
    def draw_coordinate_axis(cs, ax):
        vector_length = 0.75
        ax.plot([cs[0, 3], cs[0, 3] + cs[0, 0] * vector_length],
                [cs[1, 3], cs[1, 3] + cs[1, 0] * vector_length],
                [cs[2, 3], cs[2, 3] + cs[2, 0] * vector_length], color='red')
        ax.plot([cs[0, 3], cs[0, 3] + cs[0, 1] * vector_length],
                [cs[1, 3], cs[1, 3] + cs[1, 1] * vector_length],
                [cs[2, 3], cs[2, 3] + cs[2, 1] * vector_length], color='green')
        ax.plot([cs[0, 3], cs[0, 3] + cs[0, 2] * vector_length],
                [cs[1, 3], cs[1, 3] + cs[1, 2] * vector_length],
                [cs[2, 3], cs[2, 3] + cs[2, 2] * vector_length], color='blue')

    def plot(self):
        fig = plt.figure()
        ax = mplot3d.Axes3D(fig)
        ax.set_autoscale_on(False)
        cs = np.eye(4)
        for index, link in enumerate(self.links):
            joint_type, theta, d, a, alpha = link
            self.plot_joint(joint_type, cs, ax)
            theta = simple_float(theta, self.subs)
            d = simple_float(d, self.subs)
            a = simple_float(a, self.subs)
            alpha = simple_float(alpha, self.subs)
            next_cs = cs @ trotz(theta) @ translation([a, 0, d]) @ trotx(alpha)
            ax.plot([cs[0, 3], next_cs[0, 3]],
                    [cs[1, 3], next_cs[1, 3]],
                    [cs[2, 3], next_cs[2, 3]], color='black')
            self.draw_coordinate_axis(cs, ax)
            cs = next_cs
        self.draw_coordinate_axis(cs, ax)
        plt.show()

    def plot_joint(self, joint_type, cs, ax):
        offset = 0.5
        corrected_cs = cs @ translation([0, 0, -offset])
        axis = cs[:3, 2]
        position = corrected_cs[:3, 3]
        radius = 0.25
        mag = linalg.norm(axis)
        axis = axis / mag
        not_axis = np.array([1, 0, 0])
        if (axis == not_axis).all():
            not_axis = np.array([0, 1, 0])
        n1 = np.cross(axis, not_axis)
        n1 /= linalg.norm(n1)
        n2 = np.cross(axis, n1)
        t = np.linspace(0, mag, 2)
        theta = np.linspace(0, 2 * np.pi, 200 if joint_type == 'revolute' else 5)
        t, theta = np.meshgrid(t, theta)
        x, y, z = [position[i] + axis[i] * t + radius * np.sin(theta) * n1[i] + radius * np.cos(theta) * n2[i]
                   for i in [0, 1, 2]]
        joint_color = self.revolute_joint_color if joint_type == 'revolute' else self.prismatic_joint_color
        ax.plot_surface(x, y, z, color=joint_color)

    def transformation_matrix(self, start=None, end=None):
        sym_cs = sp.eye(4)
        start = 0 if start is None else start
        end = len(self.links) if end is None else end
        assert start in range(0, len(self.links) + 1)
        assert end in range(0, len(self.links) + 1)
        for index in range(start, end):
            joint_type, theta, d, a, alpha = self.links[index]
            theta = self.actuators[index] if joint_type == 'revolute' else theta
            d = self.actuators[index] if joint_type == 'prismatic' else d
            sym_cs = sym_cs @ sym_trotz(theta) @ sym_translation([a, 0, d]) @ sym_trotx(alpha)
            sym_cs = sp.simplify(sym_cs)
        return sp.simplify(sym_cs)

    def linear_jacobian(self):
        sym_cs = self.transformation_matrix()
        position = sym_cs[:3, 3]
        linear_jacobian = sp.Matrix([[sp.diff(output, actuator) for actuator in self.actuators] for output in position])
        return sp.simplify(linear_jacobian)

    def rotational_jacobian(self):
        sym_cs = sp.eye(4)
        rotational_jacobian = sp.Matrix([[], [], []])
        for index, link in enumerate(self.links, start=1):
            joint_type, theta, d, a, alpha = link
            theta = self.actuators[index-1] if joint_type == 'revolute' else theta
            d = self.actuators[index-1] if joint_type == 'prismatic' else d
            z_i_m1 = sym_cs[:3, 2] if joint_type == 'revolute' else sp.Matrix([[0], [0], [0]])
            rotational_jacobian = sp.Matrix.hstack(rotational_jacobian, sp.simplify(z_i_m1))
            sym_cs = sym_cs @ sym_trotz(theta) @ sym_translation([a, 0, d]) @ sym_trotx(alpha)

        return sp.simplify(rotational_jacobian)

    def jacobian(self):
        return sp.Matrix.vstack(self.linear_jacobian(), self.rotational_jacobian())

    @staticmethod
    def intertia_matrix(index):
        m = sp.symbols('m_{}'.format(index + 1))
        inr_x, inr_y, inr_z = sp.symbols('inr_x_{0}, inr_y_{0}, inr_z_{0}'.format(index + 1))
        return sp.Matrix([[0, 0, 0, 0],
                          [0, 0, 0, 0],
                          [0, 0, 0, 0],
                          [0, 0, 0, m]])

    def kinetic_energy(self):
        n = len(self.links)
        u = {}
        for i in range(n):
            for j in range(n):
                u[i, j] = sp.diff(self.transformation_matrix(0, i+1), self.actuators[j])
                u[i, j] = sp.simplify(u[i, j])

        kinetic_energy = 0
        for i in range(n):
            for p in range(i+1):
                for s in range(i+1):
                    link_kinetic_energy = sp.Matrix.trace(u[i, p] @ self.intertia_matrix(i) @ sp.transpose(u[i, s]))
                    link_kinetic_energy *= 1/2 * sp.symbols('q_{}_dot'.format(p+1)) * sp.symbols('q_{}_dot'.format(s+1))
                    kinetic_energy += sp.simplify(link_kinetic_energy)
        return kinetic_energy

    def potential_energy(self, gravity_vector, r):
        potential_energy = sp.Matrix([[0]])
        n = len(self.links)
        for i in range(n):
            link_potential_energy = -sp.symbols('m_{}'.format(i+1)) @ gravity_vector
            link_potential_energy @= (self.transformation_matrix(0, i+1) @ r[i])
            potential_energy += sp.simplify(link_potential_energy)
        return potential_energy
